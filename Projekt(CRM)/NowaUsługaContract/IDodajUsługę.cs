﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypOperacji;
using System.Data.SqlClient;

namespace NowaUsługaContract
{
    public interface IDodajUsługę: IOperacja
    {
        void DodajNowąUsługę(string NazwaUsługi);
        SqlDataReader GetUsługi();
        void PołączenieClose();
    }
}
