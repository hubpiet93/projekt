﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypOperacji;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace ObsługaKlientaContract
{
    public interface IObsługaKlienta  : IOperacja
    {
        void DodajTransakcję(SqlCommand Polecenie);
        void EdytujKlienta(string Polecenie);
       // możliwe że tego nie będzie void UsuńKlienta(int IdKlienta);
    }
}
