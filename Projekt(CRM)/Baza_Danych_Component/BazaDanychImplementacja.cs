﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using NowaUsługaContract;
using ObsługaKlientaContract;
using RejestracjaContract;
using StatystykiContract;
using WysyłanieMailiContract;
using WyświetlContract;
using HistoriaTransakcjiContract;
using TypOperacji;
namespace Baza_Danych_Component
{
    public delegate SqlConnection getPołączenie();
    public class BazaDanychImplementacja : BazaDanychContract.IListaOperacji
    {

        #region //pola
        private static SqlConnection Połączenie;
        private IDodajUsługę DodajUsługę;
        private IObsługaKlienta ObsługaKlienta;
        private IDodaj DodajKlienta;
        private IBudujStatystyki BudujStatystyki;
        private IOperacjeMail WyślijMaila;
        private IWyświetl Wyświetl;
        private IHistoriaTransakcji HistoriaTransakcji;
        private IList<IOperacja> DostępneOperacje;
        #endregion

        public BazaDanychImplementacja(IDodaj DodajKlienta, IOperacjeMail WyślijMaila, IObsługaKlienta ObsługaKlienta, IWyświetl Wyświetl, IDodajUsługę DodajUsługę, IBudujStatystyki BudujStatystyki, IHistoriaTransakcji HistoriaTransakcji) 
        {
          //  this.Połączenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=H:\repozitoryprojekt\projekt\Projekt(CRM)\Baza_Danych_Component\Baza.mdf;Integrated Security=True");
            this.DodajUsługę = DodajUsługę;
            this.ObsługaKlienta = ObsługaKlienta;
            this.DodajKlienta = DodajKlienta;
            this.BudujStatystyki = BudujStatystyki;
            this.WyślijMaila = WyślijMaila;
            this.Wyświetl = Wyświetl;
            this.HistoriaTransakcji = HistoriaTransakcji;
            
            this.DostępneOperacje = new List<IOperacja>(){
                this.DodajKlienta,
                this.WyślijMaila,
                this.ObsługaKlienta,
                this.Wyświetl,
                this.DodajUsługę,
                this.BudujStatystyki,
                this.HistoriaTransakcji
            };
            
            
            
        }
        public BazaDanychImplementacja() { }
        public static SqlConnection GetPołączenie()
        {
            Połączenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=H:\repozitoryprojekt\projekt\Projekt(CRM)\Baza_Danych_Component\Baza.mdf;Integrated Security=True");
            return Połączenie;
        }
        public IList<IOperacja> ListaOperacji
        {
            get
            {
                return DostępneOperacje;
            }
            set
            {
                DostępneOperacje = value;
            }
        }
    }
}
