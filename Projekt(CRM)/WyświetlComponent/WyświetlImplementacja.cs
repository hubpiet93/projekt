﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace WyświetlComponent
{
    public class WyświetlImplementacja :WyświetlContract.IWyświetl
    {
        private SqlConnection Połączenie;

        public WyświetlImplementacja(SqlConnection Połączenie)
        {
            this.Połączenie = Połączenie;
        }

        public void PołączenieClose()
        {
            Połączenie.Close();
        }

        public SqlDataReader WyświetlDanePersonalne(int IdKlienta)
        {
            SqlDataReader Czytnik;
            SqlCommand Kwerenda;
            string Polecenie;
            try
            {
                Połączenie.Open();
                Polecenie = String.Format("select * from [dbo].[Klienci] where IdKlienta = {0}", IdKlienta);
                Kwerenda = new SqlCommand(Polecenie);
                Kwerenda.Connection = Połączenie;
                Kwerenda.Clone();
                Czytnik = Kwerenda.ExecuteReader();
            }
            catch (Exception)
            {
                Czytnik = null; 
            }
            return Czytnik;
        }
        public SqlDataReader WyświetlWszystkichKlientów()
        {
            SqlDataReader Czytnik;
            SqlCommand Kwerenda;
            string Polecenie;
            try
            {
                Połączenie.Open();
                Polecenie = String.Format("select * from [dbo].[Klienci]");
                Kwerenda = new SqlCommand(Polecenie);
                Kwerenda.Connection = Połączenie;
                Kwerenda.Clone();
                Czytnik = Kwerenda.ExecuteReader();
            }
            catch (Exception)
            {
                Czytnik = null;
            }
            return Czytnik;
        }
        public SqlDataReader WyświetlWszystkieTransakcje()
        {
            SqlDataReader Czytnik;
            SqlCommand Kwerenda;
            string Polecenie;
            try
            {
                Polecenie = String.Format("select * from [dbo].[Transakcje]");
                Kwerenda = new SqlCommand(Polecenie);
                Kwerenda.Connection = Połączenie;
                Połączenie.Open();
                Czytnik = Kwerenda.ExecuteReader();
            }
            catch (Exception)
            {
                Czytnik = null;
            }
            return Czytnik;
        }
    }
}
