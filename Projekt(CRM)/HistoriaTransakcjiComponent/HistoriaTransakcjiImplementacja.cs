﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypOperacji;
using HistoriaTransakcjiContract;
using System.Data.SqlClient;
namespace HistoriaTransakcjiComponent
{
    public class HistoriaTransakcjiImplementacja : IHistoriaTransakcji
    {
        private SqlConnection Połączenie;

        public HistoriaTransakcjiImplementacja(SqlConnection Połączenie)
        {
            this.Połączenie = Połączenie;
        }
        public SqlDataReader HistoriaTransakcji(int IdKlienta)
        {
            SqlDataReader Czytnik;
            SqlCommand Kwerenda;
            string Polecenie;
            try
            {
                Połączenie.Open();
                Polecenie = String.Format("select * from [dbo].[Transakcje] where IdKlienta = {0}", IdKlienta);
                Kwerenda = new SqlCommand(Polecenie);
                //Polecenie.Clone();
                Czytnik = Kwerenda.ExecuteReader();
            }
            catch (Exception)
            {
                Czytnik = null;
            }
            return Czytnik;
        }
    }
}
