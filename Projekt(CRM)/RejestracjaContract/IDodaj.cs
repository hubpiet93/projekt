﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypOperacji;
namespace RejestracjaContract
{
    public interface IDodaj : IOperacja
    {
        void Dodaj(string polecenie);
    }
}
