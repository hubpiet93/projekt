﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypOperacji;
using System.Data.SqlClient;

namespace HistoriaTransakcjiContract
{
    public interface IHistoriaTransakcji : IOperacja
    {
        SqlDataReader HistoriaTransakcji(int IdKlienta);
    }
}
