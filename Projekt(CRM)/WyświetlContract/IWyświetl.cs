﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using TypOperacji;
namespace WyświetlContract
{
    public interface IWyświetl : IOperacja
    {
        SqlDataReader WyświetlDanePersonalne(int IdKlienta);
        SqlDataReader WyświetlWszystkichKlientów();
        SqlDataReader WyświetlWszystkieTransakcje();
        void PołączenieClose();
    }
}
