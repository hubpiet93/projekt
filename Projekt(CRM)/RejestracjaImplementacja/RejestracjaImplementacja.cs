﻿#region//biblioteki
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using TypOperacji;
using System.Reflection;
using System.Data.SqlClient;
#endregion
namespace RejestracjaComponent
{
    public class RejestracjaImplementacja: RejestracjaContract.IDodaj
    {
        private SqlConnection Połączenie;
        public RejestracjaImplementacja(SqlConnection Połączenie)
        {
            this.Połączenie = Połączenie;
        }

        public void Dodaj(string Polecenie)
        {
            SqlCommand kwerenda;
            try
            {
                kwerenda = new SqlCommand(Polecenie);
                Połączenie.Open();
                kwerenda.Connection = Połączenie;

                
                kwerenda.ExecuteNonQuery();
                Połączenie.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show("Dodanie klienta nie udało się");
            }
        }
    }
}
