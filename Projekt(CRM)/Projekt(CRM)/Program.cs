﻿#region//biblioteki
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PK.Container;
using NowaUsługaContract;
using ObsługaKlientaContract;
using RejestracjaContract;
using StatystykiContract;
using WysyłanieMailiContract;
using WyświetlContract;
using HistoriaTransakcjiContract;
using BazaDanychContract;
using NowaUsługaComponent;
using ObsługaKlientaComponent;
using RejestracjaComponent;
using StatystykiComponent;
using WysyłanieMailiComponent;
using WyświetlComponent;
using HistoriaTransakcjiComponent;
using Baza_Danych_Component;
using System.Data.SqlClient;
using TypOperacji;
using System.Reflection;
#endregion

namespace Projekt_CRM_
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static Kontener Kontener;
        static Object ListaOperacjiInterfejs;
        static IList<IOperacja> DostępneOperacje = new List<IOperacja>();
        [STAThread]
        static void Main()
        {
            TworzenieKontenera();
            TworzenieDostępnychOperacji();
            MainWindow.DOSTĘPNEOPERACJE = DostępneOperacje;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }

        static void TworzenieKontenera()
        {
            #region//Tworzenie Kontenera
            getPołączenie PołączenieDelagat = new getPołączenie(BazaDanychImplementacja.GetPołączenie);
            IHistoriaTransakcji HistoriaImplementacja = new HistoriaTransakcjiImplementacja(PołączenieDelagat());
            IDodajUsługę NowaUsługaImplementacja = new NowaUsługaImplementacja(PołączenieDelagat());
            IObsługaKlienta ObsługaKlientaImplementacja = new ObsługaKlientaImplementacja(PołączenieDelagat());
            IDodaj RejestracjaKlientaImplementacja = new RejestracjaImplementacja(PołączenieDelagat());
            IBudujStatystyki StatystykiImplementacja = new StatystykiImplementacja(PołączenieDelagat());
            IOperacjeMail WysyłanieMailiImplementacja = new WysyłanieMailiImplementacja();
            IWyświetl WyświetlanieImplementacja = new WyświetlImplementacja(PołączenieDelagat());
            IListaOperacji BazaImplementacja = new BazaDanychImplementacja
                (
                    RejestracjaKlientaImplementacja,
                    WysyłanieMailiImplementacja,
                    ObsługaKlientaImplementacja,
                    WyświetlanieImplementacja,
                    NowaUsługaImplementacja,
                    StatystykiImplementacja,
                    HistoriaImplementacja
                );


            Kontener = new Kontener();
            Kontener.Register(BazaImplementacja);
            Kontener.Register(RejestracjaKlientaImplementacja);
            Kontener.Register(WysyłanieMailiImplementacja);
            Kontener.Register(ObsługaKlientaImplementacja);
            Kontener.Register(WyświetlanieImplementacja);
            Kontener.Register(NowaUsługaImplementacja);
            Kontener.Register(StatystykiImplementacja);
            Kontener.Register(HistoriaImplementacja);
            #endregion      
        }
        static void TworzenieDostępnychOperacji()
        {
            ListaOperacjiInterfejs = Kontener.Resolve<IListaOperacji>();
            DostępneOperacje = (ListaOperacjiInterfejs as IListaOperacji).ListaOperacji;
        }
    }
}
