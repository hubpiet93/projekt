﻿namespace Projekt_CRM_
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
       
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.edytujKlientaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajTransakcjęToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wyślijMailaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wyświetlDaneKlientaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statystykiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historiaTransakcjiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nowaUsługaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.koniecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.DodajKomentarz = new System.Windows.Forms.Button();
            this.WyświetlDaneButton = new System.Windows.Forms.Button();
            this.DodajKlientaButton = new System.Windows.Forms.Button();
            this.KomentarzDodaj = new System.Windows.Forms.TextBox();
            this.KomentarzWyswietl = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.wyświetlanieOkno1 = new Projekt_CRM_.Kontrolki.WyświetlanieOkno();
            this.dodajTransakcjęOkno1 = new Projekt_CRM_.Kontrolki.DodajTransakcjęOkno();
            this.wyślijMailaOkno1 = new Projekt_CRM_.Kontrolki.WyślijMailaOkno();
            this.daneKlientaOkno1 = new Projekt_CRM_.Kontrolki.DaneKlientaOkno();
            this.statystykiOkno1 = new Projekt_CRM_.Kontrolki.StatystykiOkno();
            this.historiaTransakcjiOkno1 = new Projekt_CRM_.Kontrolki.HistoriaTransakcjiOkno();
            this.nowaUsługaOkno1 = new Projekt_CRM_.NowaUsługaOkno();
            this.edytujKlientaOkno1 = new Projekt_CRM_.EdytujKlientaOkno();
            this.dodajKlientaOkno1 = new Projekt_CRM_.DodajKlientaOkno();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.edytujKlientaToolStripMenuItem,
            this.dodajTransakcjęToolStripMenuItem,
            this.wyślijMailaToolStripMenuItem,
            this.wyświetlDaneKlientaToolStripMenuItem,
            this.statystykiToolStripMenuItem,
            this.historiaTransakcjiToolStripMenuItem,
            this.nowaUsługaToolStripMenuItem,
            this.koniecToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(934, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // edytujKlientaToolStripMenuItem
            // 
            this.edytujKlientaToolStripMenuItem.Name = "edytujKlientaToolStripMenuItem";
            this.edytujKlientaToolStripMenuItem.Size = new System.Drawing.Size(110, 24);
            this.edytujKlientaToolStripMenuItem.Text = "Edytuj klienta";
            this.edytujKlientaToolStripMenuItem.Click += new System.EventHandler(this.edytujKlientaToolStripMenuItem_Click);
            // 
            // dodajTransakcjęToolStripMenuItem
            // 
            this.dodajTransakcjęToolStripMenuItem.Name = "dodajTransakcjęToolStripMenuItem";
            this.dodajTransakcjęToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.dodajTransakcjęToolStripMenuItem.Text = "Dodaj transakcję";
            this.dodajTransakcjęToolStripMenuItem.Click += new System.EventHandler(this.dodajTransakcjęToolStripMenuItem_Click);
            // 
            // wyślijMailaToolStripMenuItem
            // 
            this.wyślijMailaToolStripMenuItem.Name = "wyślijMailaToolStripMenuItem";
            this.wyślijMailaToolStripMenuItem.Size = new System.Drawing.Size(101, 24);
            this.wyślijMailaToolStripMenuItem.Text = "Wyślij maila";
            this.wyślijMailaToolStripMenuItem.Click += new System.EventHandler(this.wyślijMailaToolStripMenuItem_Click);
            // 
            // wyświetlDaneKlientaToolStripMenuItem
            // 
            this.wyświetlDaneKlientaToolStripMenuItem.Name = "wyświetlDaneKlientaToolStripMenuItem";
            this.wyświetlDaneKlientaToolStripMenuItem.Size = new System.Drawing.Size(165, 24);
            this.wyświetlDaneKlientaToolStripMenuItem.Text = "Wyświetl dane klienta";
            this.wyświetlDaneKlientaToolStripMenuItem.Click += new System.EventHandler(this.wyświetlDaneKlientaToolStripMenuItem_Click);
            // 
            // statystykiToolStripMenuItem
            // 
            this.statystykiToolStripMenuItem.Name = "statystykiToolStripMenuItem";
            this.statystykiToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.statystykiToolStripMenuItem.Text = "Statystyki";
            this.statystykiToolStripMenuItem.Click += new System.EventHandler(this.statystykiToolStripMenuItem_Click);
            // 
            // historiaTransakcjiToolStripMenuItem
            // 
            this.historiaTransakcjiToolStripMenuItem.Name = "historiaTransakcjiToolStripMenuItem";
            this.historiaTransakcjiToolStripMenuItem.Size = new System.Drawing.Size(139, 24);
            this.historiaTransakcjiToolStripMenuItem.Text = "Historia transakcji";
            this.historiaTransakcjiToolStripMenuItem.Click += new System.EventHandler(this.historiaTransakcjiToolStripMenuItem_Click);
            // 
            // nowaUsługaToolStripMenuItem
            // 
            this.nowaUsługaToolStripMenuItem.Name = "nowaUsługaToolStripMenuItem";
            this.nowaUsługaToolStripMenuItem.Size = new System.Drawing.Size(107, 24);
            this.nowaUsługaToolStripMenuItem.Text = "Nowa usługa";
            this.nowaUsługaToolStripMenuItem.Click += new System.EventHandler(this.nowaUsługaToolStripMenuItem_Click);
            // 
            // koniecToolStripMenuItem
            // 
            this.koniecToolStripMenuItem.Name = "koniecToolStripMenuItem";
            this.koniecToolStripMenuItem.Size = new System.Drawing.Size(66, 24);
            this.koniecToolStripMenuItem.Text = "Koniec";
            this.koniecToolStripMenuItem.Click += new System.EventHandler(this.koniecToolStripMenuItem_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(29, 33);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 2;
            // 
            // DodajKomentarz
            // 
            this.DodajKomentarz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DodajKomentarz.Location = new System.Drawing.Point(12, 602);
            this.DodajKomentarz.Name = "DodajKomentarz";
            this.DodajKomentarz.Size = new System.Drawing.Size(187, 31);
            this.DodajKomentarz.TabIndex = 9;
            this.DodajKomentarz.Text = "Dodaj komentarz";
            this.DodajKomentarz.UseVisualStyleBackColor = true;
            // 
            // WyświetlDaneButton
            // 
            this.WyświetlDaneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WyświetlDaneButton.Location = new System.Drawing.Point(12, 244);
            this.WyświetlDaneButton.Name = "WyświetlDaneButton";
            this.WyświetlDaneButton.Size = new System.Drawing.Size(187, 31);
            this.WyświetlDaneButton.TabIndex = 8;
            this.WyświetlDaneButton.Text = "Wyświetl dane";
            this.WyświetlDaneButton.UseVisualStyleBackColor = true;
            this.WyświetlDaneButton.Click += new System.EventHandler(this.WyświetlDaneButton_Click);
            // 
            // DodajKlientaButton
            // 
            this.DodajKlientaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DodajKlientaButton.Location = new System.Drawing.Point(12, 207);
            this.DodajKlientaButton.Name = "DodajKlientaButton";
            this.DodajKlientaButton.Size = new System.Drawing.Size(187, 31);
            this.DodajKlientaButton.TabIndex = 7;
            this.DodajKlientaButton.Text = "Dodaj klienta";
            this.DodajKlientaButton.UseVisualStyleBackColor = true;
            this.DodajKlientaButton.Click += new System.EventHandler(this.DodajKlientaButton_Click);
            // 
            // KomentarzDodaj
            // 
            this.KomentarzDodaj.Location = new System.Drawing.Point(12, 434);
            this.KomentarzDodaj.Multiline = true;
            this.KomentarzDodaj.Name = "KomentarzDodaj";
            this.KomentarzDodaj.Size = new System.Drawing.Size(187, 151);
            this.KomentarzDodaj.TabIndex = 11;
            // 
            // KomentarzWyswietl
            // 
            this.KomentarzWyswietl.Enabled = false;
            this.KomentarzWyswietl.Location = new System.Drawing.Point(12, 281);
            this.KomentarzWyswietl.Multiline = true;
            this.KomentarzWyswietl.Name = "KomentarzWyswietl";
            this.KomentarzWyswietl.Size = new System.Drawing.Size(187, 134);
            this.KomentarzWyswietl.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.wyświetlanieOkno1);
            this.groupBox1.Controls.Add(this.dodajTransakcjęOkno1);
            this.groupBox1.Controls.Add(this.wyślijMailaOkno1);
            this.groupBox1.Controls.Add(this.daneKlientaOkno1);
            this.groupBox1.Controls.Add(this.statystykiOkno1);
            this.groupBox1.Controls.Add(this.historiaTransakcjiOkno1);
            this.groupBox1.Controls.Add(this.nowaUsługaOkno1);
            this.groupBox1.Controls.Add(this.edytujKlientaOkno1);
            this.groupBox1.Controls.Add(this.dodajKlientaOkno1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(227, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(670, 600);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // wyświetlanieOkno1
            // 
            this.wyświetlanieOkno1.Location = new System.Drawing.Point(4, 14);
            this.wyświetlanieOkno1.Name = "wyświetlanieOkno1";
            this.wyświetlanieOkno1.Size = new System.Drawing.Size(660, 580);
            this.wyświetlanieOkno1.TabIndex = 9;
            this.wyświetlanieOkno1.Visible = false;
            // 
            // dodajTransakcjęOkno1
            // 
            this.dodajTransakcjęOkno1.Location = new System.Drawing.Point(4, 14);
            this.dodajTransakcjęOkno1.Name = "dodajTransakcjęOkno1";
            this.dodajTransakcjęOkno1.Size = new System.Drawing.Size(660, 580);
            this.dodajTransakcjęOkno1.TabIndex = 8;
            this.dodajTransakcjęOkno1.Visible = false;
            // 
            // wyślijMailaOkno1
            // 
            this.wyślijMailaOkno1.Location = new System.Drawing.Point(6, 14);
            this.wyślijMailaOkno1.Name = "wyślijMailaOkno1";
            this.wyślijMailaOkno1.Size = new System.Drawing.Size(660, 580);
            this.wyślijMailaOkno1.TabIndex = 7;
            this.wyślijMailaOkno1.Visible = false;
            // 
            // daneKlientaOkno1
            // 
            this.daneKlientaOkno1.Location = new System.Drawing.Point(6, 14);
            this.daneKlientaOkno1.Name = "daneKlientaOkno1";
            this.daneKlientaOkno1.Size = new System.Drawing.Size(660, 580);
            this.daneKlientaOkno1.TabIndex = 6;
            this.daneKlientaOkno1.Visible = false;
            // 
            // statystykiOkno1
            // 
            this.statystykiOkno1.Location = new System.Drawing.Point(6, 14);
            this.statystykiOkno1.Name = "statystykiOkno1";
            this.statystykiOkno1.Size = new System.Drawing.Size(660, 580);
            this.statystykiOkno1.TabIndex = 5;
            this.statystykiOkno1.Visible = false;
            // 
            // historiaTransakcjiOkno1
            // 
            this.historiaTransakcjiOkno1.Location = new System.Drawing.Point(4, 14);
            this.historiaTransakcjiOkno1.Name = "historiaTransakcjiOkno1";
            this.historiaTransakcjiOkno1.Size = new System.Drawing.Size(660, 580);
            this.historiaTransakcjiOkno1.TabIndex = 4;
            this.historiaTransakcjiOkno1.Visible = false;
            // 
            // nowaUsługaOkno1
            // 
            this.nowaUsługaOkno1.Location = new System.Drawing.Point(4, 14);
            this.nowaUsługaOkno1.Name = "nowaUsługaOkno1";
            this.nowaUsługaOkno1.Size = new System.Drawing.Size(660, 580);
            this.nowaUsługaOkno1.TabIndex = 3;
            this.nowaUsługaOkno1.Visible = false;
            // 
            // edytujKlientaOkno1
            // 
            this.edytujKlientaOkno1.Location = new System.Drawing.Point(4, 14);
            this.edytujKlientaOkno1.Name = "edytujKlientaOkno1";
            this.edytujKlientaOkno1.Size = new System.Drawing.Size(660, 580);
            this.edytujKlientaOkno1.TabIndex = 2;
            this.edytujKlientaOkno1.Visible = false;
            // 
            // dodajKlientaOkno1
            // 
            this.dodajKlientaOkno1.Location = new System.Drawing.Point(4, 14);
            this.dodajKlientaOkno1.Name = "dodajKlientaOkno1";
            this.dodajKlientaOkno1.Size = new System.Drawing.Size(660, 580);
            this.dodajKlientaOkno1.TabIndex = 1;
            this.dodajKlientaOkno1.Visible = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 662);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.KomentarzDodaj);
            this.Controls.Add(this.KomentarzWyswietl);
            this.Controls.Add(this.DodajKomentarz);
            this.Controls.Add(this.WyświetlDaneButton);
            this.Controls.Add(this.DodajKlientaButton);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainWindow";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem edytujKlientaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajTransakcjęToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wyślijMailaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wyświetlDaneKlientaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statystykiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historiaTransakcjiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nowaUsługaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem koniecToolStripMenuItem;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button DodajKomentarz;
        private System.Windows.Forms.Button WyświetlDaneButton;
        private System.Windows.Forms.Button DodajKlientaButton;
        private System.Windows.Forms.TextBox KomentarzDodaj;
        private System.Windows.Forms.TextBox KomentarzWyswietl;
        private System.Windows.Forms.GroupBox groupBox1;
        
        private DodajKlientaOkno dodajKlientaOkno1;
        private EdytujKlientaOkno edytujKlientaOkno1;
        private NowaUsługaOkno nowaUsługaOkno1;
        private Kontrolki.HistoriaTransakcjiOkno historiaTransakcjiOkno1;
        private Kontrolki.StatystykiOkno statystykiOkno1;
        private Kontrolki.DaneKlientaOkno daneKlientaOkno1;
        private Kontrolki.WyślijMailaOkno wyślijMailaOkno1;
        private Kontrolki.DodajTransakcjęOkno dodajTransakcjęOkno1;
        private Kontrolki.WyświetlanieOkno wyświetlanieOkno1;
    }
}