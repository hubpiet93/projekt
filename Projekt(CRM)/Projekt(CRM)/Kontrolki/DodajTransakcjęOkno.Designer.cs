﻿namespace Projekt_CRM_.Kontrolki
{
    partial class DodajTransakcjęOkno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CofnijButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ComboKlienci = new System.Windows.Forms.ComboBox();
            this.UsługaCombo = new System.Windows.Forms.ComboBox();
            this.FormaPłatnościCombo = new System.Windows.Forms.ComboBox();
            this.KosztNumeric = new System.Windows.Forms.NumericUpDown();
            this.pln = new System.Windows.Forms.Label();
            this.DataTransakcji = new System.Windows.Forms.DateTimePicker();
            this.DodajTransakcję = new System.Windows.Forms.Button();
            this.DataPatności = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.KosztNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // CofnijButton
            // 
            this.CofnijButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CofnijButton.Location = new System.Drawing.Point(3, 3);
            this.CofnijButton.Name = "CofnijButton";
            this.CofnijButton.Size = new System.Drawing.Size(75, 23);
            this.CofnijButton.TabIndex = 7;
            this.CofnijButton.Text = "Cofnij";
            this.CofnijButton.UseVisualStyleBackColor = true;
            this.CofnijButton.Click += new System.EventHandler(this.CofnijButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(271, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Dodaj transakcję";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(148, 292);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 17);
            this.label7.TabIndex = 19;
            this.label7.Text = "Forma płatności:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(148, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Data transakcji:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(148, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Koszt usługi:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(148, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Usługa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(148, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Klient:";
            // 
            // ComboKlienci
            // 
            this.ComboKlienci.FormattingEnabled = true;
            this.ComboKlienci.Location = new System.Drawing.Point(274, 105);
            this.ComboKlienci.Name = "ComboKlienci";
            this.ComboKlienci.Size = new System.Drawing.Size(200, 21);
            this.ComboKlienci.TabIndex = 20;
            // 
            // UsługaCombo
            // 
            this.UsługaCombo.FormattingEnabled = true;
            this.UsługaCombo.Location = new System.Drawing.Point(274, 139);
            this.UsługaCombo.Name = "UsługaCombo";
            this.UsługaCombo.Size = new System.Drawing.Size(200, 21);
            this.UsługaCombo.TabIndex = 21;
            // 
            // FormaPłatnościCombo
            // 
            this.FormaPłatnościCombo.FormattingEnabled = true;
            this.FormaPłatnościCombo.Location = new System.Drawing.Point(274, 292);
            this.FormaPłatnościCombo.Name = "FormaPłatnościCombo";
            this.FormaPłatnościCombo.Size = new System.Drawing.Size(200, 21);
            this.FormaPłatnościCombo.TabIndex = 22;
            // 
            // KosztNumeric
            // 
            this.KosztNumeric.Location = new System.Drawing.Point(274, 177);
            this.KosztNumeric.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.KosztNumeric.Name = "KosztNumeric";
            this.KosztNumeric.Size = new System.Drawing.Size(200, 20);
            this.KosztNumeric.TabIndex = 23;
            // 
            // pln
            // 
            this.pln.AutoSize = true;
            this.pln.Location = new System.Drawing.Point(480, 182);
            this.pln.Name = "pln";
            this.pln.Size = new System.Drawing.Size(21, 13);
            this.pln.TabIndex = 24;
            this.pln.Text = "pln";
            // 
            // DataTransakcji
            // 
            this.DataTransakcji.CustomFormat = "yyyy-MM-dd";
            this.DataTransakcji.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DataTransakcji.Location = new System.Drawing.Point(274, 217);
            this.DataTransakcji.Name = "DataTransakcji";
            this.DataTransakcji.Size = new System.Drawing.Size(200, 20);
            this.DataTransakcji.TabIndex = 25;
            // 
            // DodajTransakcję
            // 
            this.DodajTransakcję.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DodajTransakcję.Location = new System.Drawing.Point(274, 330);
            this.DodajTransakcję.Name = "DodajTransakcję";
            this.DodajTransakcję.Size = new System.Drawing.Size(111, 40);
            this.DodajTransakcję.TabIndex = 26;
            this.DodajTransakcję.Text = "Dodaj transakcję";
            this.DodajTransakcję.UseVisualStyleBackColor = true;
            this.DodajTransakcję.Click += new System.EventHandler(this.DodajTransakcję_Click);
            // 
            // DataPatności
            // 
            this.DataPatności.Location = new System.Drawing.Point(274, 257);
            this.DataPatności.Name = "DataPatności";
            this.DataPatności.Size = new System.Drawing.Size(200, 20);
            this.DataPatności.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(148, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 17);
            this.label5.TabIndex = 27;
            this.label5.Text = "Data płatności:";
            // 
            // DodajTransakcjęOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DataPatności);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DodajTransakcję);
            this.Controls.Add(this.DataTransakcji);
            this.Controls.Add(this.pln);
            this.Controls.Add(this.KosztNumeric);
            this.Controls.Add(this.FormaPłatnościCombo);
            this.Controls.Add(this.UsługaCombo);
            this.Controls.Add(this.ComboKlienci);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CofnijButton);
            this.Name = "DodajTransakcjęOkno";
            this.Size = new System.Drawing.Size(660, 580);
            ((System.ComponentModel.ISupportInitialize)(this.KosztNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CofnijButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ComboKlienci;
        private System.Windows.Forms.ComboBox UsługaCombo;
        private System.Windows.Forms.ComboBox FormaPłatnościCombo;
        private System.Windows.Forms.NumericUpDown KosztNumeric;
        private System.Windows.Forms.Label pln;
        private System.Windows.Forms.DateTimePicker DataTransakcji;
        private System.Windows.Forms.Button DodajTransakcję;
        private System.Windows.Forms.DateTimePicker DataPatności;
        private System.Windows.Forms.Label label5;
    }
}
