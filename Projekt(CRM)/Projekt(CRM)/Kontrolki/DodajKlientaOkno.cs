﻿#region//biblioteki
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PK.Container;
using NowaUsługaContract;
using ObsługaKlientaContract;
using RejestracjaContract;
using StatystykiContract;
using WysyłanieMailiContract;
using WyświetlContract;
using HistoriaTransakcjiContract;
using BazaDanychContract;
using NowaUsługaComponent;
using ObsługaKlientaComponent;
using RejestracjaComponent;
using StatystykiComponent;
using WysyłanieMailiComponent;
using WyświetlComponent;
using HistoriaTransakcjiComponent;
using Baza_Danych_Component;
using System.Text.RegularExpressions;
using TypOperacji;
using System.Reflection;
using System.Data.SqlClient;
#endregion

namespace Projekt_CRM_
{
    public partial class DodajKlientaOkno : UserControl
    {
        
        public DodajKlientaOkno()
        {
            InitializeComponent();
        }

        private void CofnijButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void DodajButton_Click(object sender, EventArgs e)
        {
            if (!(Imię.Text == "" || Pesel.Text == "" || KodPocztowy.Text == "" || Miasto.Text == "" || Ulica.Text == "" || NumerDomu.Text == ""))
            {
                string PolecenieSql = String.Format(
                                       @"insert into [dbo].[Klienci]
                                       ([Imię/NazwaFirmy],[Nazwisko],[PESEL/REGON],[NIP],[Kod],[Miasto],[Ulica],[NumerDomu],[NumerMieszkania] ,[EMail],[Telefon])
                                       values
                                       ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')", Imię.Text, Nazwisko.Text, Pesel.Text, Nip.Text, KodPocztowy.Text, Miasto.Text, Ulica.Text, NumerDomu.Text, NumerMieszkania.Text, EMail.Text, Telefon.Text);
                try
                {
                    (MainWindow.DOSTĘPNEOPERACJE[0] as IDodaj).Dodaj(PolecenieSql);
                    Imię.Text = "";
                    Nazwisko.Text = "";
                    Pesel.Text = "";
                    Nip.Text = "";
                    KodPocztowy.Text = "";
                    Miasto.Text = "";
                    Ulica.Text = "";
                    NumerDomu.Text = "";
                    NumerMieszkania.Text = "";
                    EMail.Text = "";
                    Telefon.Text = "";
                }
                catch (Exception Error)
                {
                    MessageBox.Show(Error.Message);
                }
            }
        }

        public bool SprawdzeniePoprawności()
        {
            bool wynik = false;
            Regex regex = new Regex(@"^[^ ][a-zA-Z ]+[^ ]$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            if (!regex.IsMatch(Imię.Text))
            {
                PoprawneImię.Text = "***";
            }
            else
            {
                PoprawneImię.Text = "";
            }
              return wynik;
        }
    }
}
