﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_CRM_.Kontrolki
{
    public partial class WyślijMailaOkno : UserControl
    {
        public WyślijMailaOkno()
        {
            InitializeComponent();
        }

        private void CofnijButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void DoWszystkich_CheckedChanged(object sender, EventArgs e)
        {
            if (DoWszystkich.Checked == true)
            {
                AdresOdbiorcy.Enabled = false;
            }
            else
            {
                AdresOdbiorcy.Enabled = true;
            }
        }
    }
}
