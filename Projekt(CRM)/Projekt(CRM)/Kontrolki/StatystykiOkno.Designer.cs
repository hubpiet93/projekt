﻿namespace Projekt_CRM_.Kontrolki
{
    partial class StatystykiOkno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CofnijButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.KlientCombo = new System.Windows.Forms.ComboBox();
            this.PrzychódZKlientaLista = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PrzychódKwartalnyLista = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // CofnijButton
            // 
            this.CofnijButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CofnijButton.Location = new System.Drawing.Point(3, 3);
            this.CofnijButton.Name = "CofnijButton";
            this.CofnijButton.Size = new System.Drawing.Size(75, 23);
            this.CofnijButton.TabIndex = 4;
            this.CofnijButton.Text = "Cofnij";
            this.CofnijButton.UseVisualStyleBackColor = true;
            this.CofnijButton.Click += new System.EventHandler(this.CofnijButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(274, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Statystyki";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(28, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Przychód z klienta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(19, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Wybierz klienta:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PrzychódZKlientaLista);
            this.groupBox1.Controls.Add(this.KlientCombo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(157, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(483, 246);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // KlientCombo
            // 
            this.KlientCombo.FormattingEnabled = true;
            this.KlientCombo.Location = new System.Drawing.Point(160, 26);
            this.KlientCombo.Name = "KlientCombo";
            this.KlientCombo.Size = new System.Drawing.Size(270, 21);
            this.KlientCombo.TabIndex = 9;
            // 
            // PrzychódZKlientaLista
            // 
            this.PrzychódZKlientaLista.FormattingEnabled = true;
            this.PrzychódZKlientaLista.Location = new System.Drawing.Point(22, 65);
            this.PrzychódZKlientaLista.Name = "PrzychódZKlientaLista";
            this.PrzychódZKlientaLista.Size = new System.Drawing.Size(446, 160);
            this.PrzychódZKlientaLista.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(28, 348);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Przychó kwartalny";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PrzychódKwartalnyLista);
            this.groupBox2.Location = new System.Drawing.Point(157, 348);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(483, 193);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            // 
            // PrzychódKwartalnyLista
            // 
            this.PrzychódKwartalnyLista.FormattingEnabled = true;
            this.PrzychódKwartalnyLista.Location = new System.Drawing.Point(19, 16);
            this.PrzychódKwartalnyLista.Name = "PrzychódKwartalnyLista";
            this.PrzychódKwartalnyLista.Size = new System.Drawing.Size(449, 160);
            this.PrzychódKwartalnyLista.TabIndex = 11;
            // 
            // StatystykiOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CofnijButton);
            this.Name = "StatystykiOkno";
            this.Size = new System.Drawing.Size(660, 580);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CofnijButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox PrzychódZKlientaLista;
        private System.Windows.Forms.ComboBox KlientCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox PrzychódKwartalnyLista;
    }
}
