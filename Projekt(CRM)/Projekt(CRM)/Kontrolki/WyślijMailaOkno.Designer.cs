﻿namespace Projekt_CRM_.Kontrolki
{
    partial class WyślijMailaOkno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CofnijButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.AdresNadawcy = new System.Windows.Forms.TextBox();
            this.AdresOdbiorcy = new System.Windows.Forms.TextBox();
            this.AdresSmtp = new System.Windows.Forms.TextBox();
            this.Temat = new System.Windows.Forms.TextBox();
            this.Hasło = new System.Windows.Forms.TextBox();
            this.DoWszystkich = new System.Windows.Forms.CheckBox();
            this.Wiadomość = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.WyślijButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CofnijButton
            // 
            this.CofnijButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CofnijButton.Location = new System.Drawing.Point(3, 3);
            this.CofnijButton.Name = "CofnijButton";
            this.CofnijButton.Size = new System.Drawing.Size(75, 23);
            this.CofnijButton.TabIndex = 6;
            this.CofnijButton.Text = "Cofnij";
            this.CofnijButton.UseVisualStyleBackColor = true;
            this.CofnijButton.Click += new System.EventHandler(this.CofnijButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(295, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Wyślij maila";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(159, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Z:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(159, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Do:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(159, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Adres smtp:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(159, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Hasło:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(159, 247);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "Temat:";
            // 
            // AdresNadawcy
            // 
            this.AdresNadawcy.Location = new System.Drawing.Point(264, 99);
            this.AdresNadawcy.Name = "AdresNadawcy";
            this.AdresNadawcy.Size = new System.Drawing.Size(217, 20);
            this.AdresNadawcy.TabIndex = 15;
            // 
            // AdresOdbiorcy
            // 
            this.AdresOdbiorcy.Location = new System.Drawing.Point(264, 136);
            this.AdresOdbiorcy.Name = "AdresOdbiorcy";
            this.AdresOdbiorcy.Size = new System.Drawing.Size(217, 20);
            this.AdresOdbiorcy.TabIndex = 16;
            // 
            // AdresSmtp
            // 
            this.AdresSmtp.Location = new System.Drawing.Point(264, 173);
            this.AdresSmtp.Name = "AdresSmtp";
            this.AdresSmtp.Size = new System.Drawing.Size(217, 20);
            this.AdresSmtp.TabIndex = 17;
            // 
            // Temat
            // 
            this.Temat.Location = new System.Drawing.Point(264, 247);
            this.Temat.Name = "Temat";
            this.Temat.Size = new System.Drawing.Size(217, 20);
            this.Temat.TabIndex = 20;
            // 
            // Hasło
            // 
            this.Hasło.Location = new System.Drawing.Point(264, 210);
            this.Hasło.Name = "Hasło";
            this.Hasło.Size = new System.Drawing.Size(217, 20);
            this.Hasło.TabIndex = 19;
            // 
            // DoWszystkich
            // 
            this.DoWszystkich.AutoSize = true;
            this.DoWszystkich.Location = new System.Drawing.Point(502, 139);
            this.DoWszystkich.Name = "DoWszystkich";
            this.DoWszystkich.Size = new System.Drawing.Size(94, 17);
            this.DoWszystkich.TabIndex = 21;
            this.DoWszystkich.Text = "Do wszystkich";
            this.DoWszystkich.UseVisualStyleBackColor = true;
            this.DoWszystkich.CheckedChanged += new System.EventHandler(this.DoWszystkich_CheckedChanged);
            // 
            // Wiadomość
            // 
            this.Wiadomość.Location = new System.Drawing.Point(162, 330);
            this.Wiadomość.Multiline = true;
            this.Wiadomość.Name = "Wiadomość";
            this.Wiadomość.Size = new System.Drawing.Size(434, 152);
            this.Wiadomość.TabIndex = 22;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label.Location = new System.Drawing.Point(337, 301);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(81, 17);
            this.label.TabIndex = 23;
            this.label.Text = "Wiadomość";
            // 
            // WyślijButton
            // 
            this.WyślijButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WyślijButton.Location = new System.Drawing.Point(340, 488);
            this.WyślijButton.Name = "WyślijButton";
            this.WyślijButton.Size = new System.Drawing.Size(78, 40);
            this.WyślijButton.TabIndex = 24;
            this.WyślijButton.Text = "Wyślij";
            this.WyślijButton.UseVisualStyleBackColor = true;
            // 
            // WyślijMailaOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.WyślijButton);
            this.Controls.Add(this.label);
            this.Controls.Add(this.Wiadomość);
            this.Controls.Add(this.DoWszystkich);
            this.Controls.Add(this.Temat);
            this.Controls.Add(this.Hasło);
            this.Controls.Add(this.AdresSmtp);
            this.Controls.Add(this.AdresOdbiorcy);
            this.Controls.Add(this.AdresNadawcy);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CofnijButton);
            this.Name = "WyślijMailaOkno";
            this.Size = new System.Drawing.Size(660, 580);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CofnijButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox AdresNadawcy;
        private System.Windows.Forms.TextBox AdresOdbiorcy;
        private System.Windows.Forms.TextBox AdresSmtp;
        private System.Windows.Forms.TextBox Temat;
        private System.Windows.Forms.TextBox Hasło;
        private System.Windows.Forms.CheckBox DoWszystkich;
        private System.Windows.Forms.TextBox Wiadomość;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Button WyślijButton;
    }
}
