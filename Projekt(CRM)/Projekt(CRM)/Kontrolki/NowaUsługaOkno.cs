﻿#region//biblioteki
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PK.Container;
using NowaUsługaContract;
using ObsługaKlientaContract;
using RejestracjaContract;
using StatystykiContract;
using WysyłanieMailiContract;
using WyświetlContract;
using HistoriaTransakcjiContract;
using BazaDanychContract;
using NowaUsługaComponent;
using ObsługaKlientaComponent;
using RejestracjaComponent;
using StatystykiComponent;
using WysyłanieMailiComponent;
using WyświetlComponent;
using HistoriaTransakcjiComponent;
using Baza_Danych_Component;
using System.Text.RegularExpressions;
using TypOperacji;
using System.Reflection;
using System.Data.SqlClient;
#endregion

namespace Projekt_CRM_
{
    public partial class NowaUsługaOkno : UserControl
    {
        public NowaUsługaOkno()
        {
            InitializeComponent();
        }

        private void CofnijButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void DodajButton_Click(object sender, System.EventArgs e)
        {
            if (!(NowaUsługa.Text == ""))
            {
                try
                {
                    (MainWindow.DOSTĘPNEOPERACJE[4] as IDodajUsługę).DodajNowąUsługę(NowaUsługa.Text) ;
                    NowaUsługa.Text = "";
                }
                catch (Exception Error)
                {
                    MessageBox.Show(Error.Message);
                }
            }
        }
    }
}
