﻿namespace Projekt_CRM_.Kontrolki
{
    partial class HistoriaTransakcjiOkno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CofnijButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.WybierzKlientaCombo = new System.Windows.Forms.ComboBox();
            this.Transakcje = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // CofnijButton
            // 
            this.CofnijButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CofnijButton.Location = new System.Drawing.Point(3, 3);
            this.CofnijButton.Name = "CofnijButton";
            this.CofnijButton.Size = new System.Drawing.Size(75, 23);
            this.CofnijButton.TabIndex = 3;
            this.CofnijButton.Text = "Cofnij";
            this.CofnijButton.UseVisualStyleBackColor = true;
            this.CofnijButton.Click += new System.EventHandler(this.CofnijButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(98, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Wybierz klienta:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(240, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Historia transakcji";
            // 
            // WybierzKlientaCombo
            // 
            this.WybierzKlientaCombo.FormattingEnabled = true;
            this.WybierzKlientaCombo.Location = new System.Drawing.Point(239, 91);
            this.WybierzKlientaCombo.Name = "WybierzKlientaCombo";
            this.WybierzKlientaCombo.Size = new System.Drawing.Size(216, 21);
            this.WybierzKlientaCombo.TabIndex = 7;
            // 
            // Transakcje
            // 
            this.Transakcje.FormattingEnabled = true;
            this.Transakcje.Location = new System.Drawing.Point(35, 132);
            this.Transakcje.MultiColumn = true;
            this.Transakcje.Name = "Transakcje";
            this.Transakcje.Size = new System.Drawing.Size(585, 225);
            this.Transakcje.TabIndex = 8;
            // 
            // HistoriaTransakcjiOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Transakcje);
            this.Controls.Add(this.WybierzKlientaCombo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CofnijButton);
            this.Name = "HistoriaTransakcjiOkno";
            this.Size = new System.Drawing.Size(660, 580);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CofnijButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox WybierzKlientaCombo;
        private System.Windows.Forms.ListBox Transakcje;
    }
}
