﻿#region//biblioteki
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PK.Container;
using NowaUsługaContract;
using ObsługaKlientaContract;
using RejestracjaContract;
using StatystykiContract;
using WysyłanieMailiContract;
using WyświetlContract;
using HistoriaTransakcjiContract;
using BazaDanychContract;
using NowaUsługaComponent;
using ObsługaKlientaComponent;
using RejestracjaComponent;
using StatystykiComponent;
using WysyłanieMailiComponent;
using WyświetlComponent;
using HistoriaTransakcjiComponent;
using Baza_Danych_Component;
using System.Text.RegularExpressions;
using TypOperacji;
using System.Reflection;
using System.Data.SqlClient;
using System.Data.SqlTypes;
#endregion

namespace Projekt_CRM_.Kontrolki
{
    public partial class DodajTransakcjęOkno : UserControl
    {
        private SqlDataReader Czytacz;
        private List<int> ListaIndeksówKlienci;
        private List<int> ListaIndeksówUsługi;
        private int wybranyklient;
        private int wybranausługa;
        private bool wybieranie = false;

        public DodajTransakcjęOkno()
        {
            ListaIndeksówKlienci = new List<int>();
            ListaIndeksówUsługi = new List<int>();
            InitializeComponent();
            WypełnijComboUsługi();
            WypełnijComboKlienci();
            FormaPłatnościCombo.Items.Add("Gotówka");
            FormaPłatnościCombo.Items.Add("Karta płatnicza");


        
        }

        private void CofnijButton_Click(object sender, EventArgs e)
        {
            CzyśćPola();
            this.Visible = false;
        }

        private void DodajTransakcję_Click(object sender, EventArgs e)
        {
            
            if (!(ComboKlienci.SelectedIndex ==-1||UsługaCombo.SelectedIndex ==-1||KosztNumeric.Value==0||FormaPłatnościCombo.SelectedIndex ==-1))
            {
                SqlDateTime datat = new SqlDateTime(DataTransakcji.Value.Year, DataTransakcji.Value.Month, DataTransakcji.Value.Day);
                SqlDateTime datap = new SqlDateTime(DataPatności.Value.Year, DataPatności.Value.Month, DataPatności.Value.Day);
                
               // "CONVERT(Float,{rok})-CONVERT(Float,{miesiąć})-CONVERT(Float,{dzień})"
                wybranyklient = ListaIndeksówKlienci[ComboKlienci.SelectedIndex];
                wybranausługa = ListaIndeksówUsługi[UsługaCombo.SelectedIndex];
                string PolecenieSql = String.Format(@"insert into [dbo].[Transakcje]
                                                     ([IdKlienta],[IdUsługi],[DataTransakcji],[FormaPłatności],[WartośćTransakcji] )
                                                     values
                                                     ('" + wybranyklient + "','" + wybranausługa + "','Convert(Date," + DataTransakcji.Text + ")','" + FormaPłatnościCombo.Text + "','" +KosztNumeric.Value + "')");
                SqlCommand s = new SqlCommand(PolecenieSql);
             //   SqlDateTime t = new SqlDateTime(1993,2,3);
             //
             //   MessageBox.Show(t.ToString());
             //
             //   s.Parameters.Add("@param1", SqlDbType.Int).Value = wybranyklient;
             //   s.Parameters.Add("@param2", SqlDbType.Int).Value = wybranausługa;
             //   s.Parameters.Add("@param3", SqlDbType.DateTime).Value = DataTransakcji.Text;
             //   s.Parameters.Add("@param4", SqlDbType.VarChar).Value = FormaPłatnościCombo.SelectedItem.ToString();
             //   s.Parameters.Add("@param5", SqlDbType.Float).Value = Convert.ToDouble(KosztNumeric.Value);
             //   s.CommandType = CommandType.Text;

                try
                {
                    (MainWindow.DOSTĘPNEOPERACJE[2] as IObsługaKlienta).DodajTransakcję(s);
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message);
                    throw;
                }
                CzyśćPola();
            
            }


        }
        private void WypełnijComboKlienci()
        {
            ComboKlienci.Items.Clear();
            ComboKlienci.SelectedItem = null;
            Czytacz = (MainWindow.DOSTĘPNEOPERACJE[3] as IWyświetl).WyświetlWszystkichKlientów();
            if (Czytacz.HasRows)
            {
                while (Czytacz.Read())
                {
                    ListaIndeksówKlienci.Add(Convert.ToInt32(Czytacz[0]));
                    ComboKlienci.Items.Add("Id: " + Czytacz[0] + " " + Czytacz[1] + " " + Czytacz[2] + " Pesel: " + Czytacz[3]);
                }
            }
            (MainWindow.DOSTĘPNEOPERACJE[3] as IWyświetl).PołączenieClose();
        }
        private void WypełnijComboUsługi()
        {
            UsługaCombo.Items.Clear();
            UsługaCombo.SelectedItem = null;
            Czytacz = (MainWindow.DOSTĘPNEOPERACJE[4] as IDodajUsługę).GetUsługi();
            if (Czytacz.HasRows)
            {
                while (Czytacz.Read())
                {
                    ListaIndeksówUsługi.Add(Convert.ToInt32(Czytacz[0]));
                    UsługaCombo.Items.Add(Czytacz[1]);
                }
            }
            (MainWindow.DOSTĘPNEOPERACJE[4] as IDodajUsługę).PołączenieClose();
        }
        private void CzyśćPola()
        {
            ComboKlienci.SelectedItem = null;
            UsługaCombo.SelectedItem = null;
            KosztNumeric.Value = 0;
            DataTransakcji.ResetText();
            DataPatności.ResetText();
            FormaPłatnościCombo.SelectedItem = null;
            FormaPłatnościCombo.Text = "";
            UsługaCombo.Text = "";
            ComboKlienci.Text = "";
        }
    }
}
