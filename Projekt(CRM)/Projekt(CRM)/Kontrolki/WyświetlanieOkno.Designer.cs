﻿namespace Projekt_CRM_.Kontrolki
{
    partial class WyświetlanieOkno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.WyświetlButton = new System.Windows.Forms.Button();
            this.ListaKlientów = new System.Windows.Forms.ListView();
            this.listView2 = new System.Windows.Forms.ListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Imię = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Nazwisko = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Pesel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Kod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Miasto = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Ulica = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Dom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Mieszkanie = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Email = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Telefon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(10, 322);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Historia ostatnich transakcji";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(10, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Klienci:";
            // 
            // WyświetlButton
            // 
            this.WyświetlButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WyświetlButton.Location = new System.Drawing.Point(242, 283);
            this.WyświetlButton.Name = "WyświetlButton";
            this.WyświetlButton.Size = new System.Drawing.Size(140, 40);
            this.WyświetlButton.TabIndex = 24;
            this.WyświetlButton.Text = "Wyświetl dane";
            this.WyświetlButton.UseVisualStyleBackColor = true;
            this.WyświetlButton.Click += new System.EventHandler(this.WyświetlButton_Click);
            // 
            // ListaKlientów
            // 
            this.ListaKlientów.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.Imię,
            this.Nazwisko,
            this.Pesel,
            this.NIP,
            this.Kod,
            this.Miasto,
            this.Ulica,
            this.Dom,
            this.Mieszkanie,
            this.Email,
            this.Telefon});
            this.ListaKlientów.GridLines = true;
            this.ListaKlientów.HideSelection = false;
            this.ListaKlientów.Location = new System.Drawing.Point(13, 73);
            this.ListaKlientów.MultiSelect = false;
            this.ListaKlientów.Name = "ListaKlientów";
            this.ListaKlientów.Size = new System.Drawing.Size(635, 192);
            this.ListaKlientów.TabIndex = 25;
            this.ListaKlientów.UseCompatibleStateImageBehavior = false;
            this.ListaKlientów.View = System.Windows.Forms.View.Details;
            // 
            // listView2
            // 
            this.listView2.Location = new System.Drawing.Point(13, 342);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(635, 205);
            this.listView2.TabIndex = 26;
            this.listView2.UseCompatibleStateImageBehavior = false;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 27;
            // 
            // Imię
            // 
            this.Imię.Text = "Imię/Nazwa firmy";
            this.Imię.Width = 97;
            // 
            // Nazwisko
            // 
            this.Nazwisko.Text = "Nazwisko";
            // 
            // Pesel
            // 
            this.Pesel.Text = "Pesel";
            this.Pesel.Width = 44;
            // 
            // NIP
            // 
            this.NIP.Text = "NIP";
            this.NIP.Width = 57;
            // 
            // Kod
            // 
            this.Kod.Text = "Kod pocztowy";
            this.Kod.Width = 86;
            // 
            // Miasto
            // 
            this.Miasto.Text = "Miasto";
            this.Miasto.Width = 47;
            // 
            // Ulica
            // 
            this.Ulica.Text = "Ulica";
            this.Ulica.Width = 45;
            // 
            // Dom
            // 
            this.Dom.Text = "Numer domu";
            this.Dom.Width = 76;
            // 
            // Mieszkanie
            // 
            this.Mieszkanie.Text = "Numer mieszkania";
            this.Mieszkanie.Width = 108;
            // 
            // Email
            // 
            this.Email.Text = "Adres email";
            this.Email.Width = 73;
            // 
            // Telefon
            // 
            this.Telefon.Text = "Telefon";
            this.Telefon.Width = 52;
            // 
            // WyświetlanieOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.ListaKlientów);
            this.Controls.Add(this.WyświetlButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "WyświetlanieOkno";
            this.Size = new System.Drawing.Size(660, 580);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button WyświetlButton;
        private System.Windows.Forms.ListView ListaKlientów;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader Imię;
        private System.Windows.Forms.ColumnHeader Nazwisko;
        private System.Windows.Forms.ColumnHeader Pesel;
        private System.Windows.Forms.ColumnHeader NIP;
        private System.Windows.Forms.ColumnHeader Kod;
        private System.Windows.Forms.ColumnHeader Miasto;
        private System.Windows.Forms.ColumnHeader Ulica;
        private System.Windows.Forms.ColumnHeader Dom;
        private System.Windows.Forms.ColumnHeader Mieszkanie;
        private System.Windows.Forms.ColumnHeader Email;
        private System.Windows.Forms.ColumnHeader Telefon;
    }
}
