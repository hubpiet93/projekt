﻿#region//biblioteki
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PK.Container;
using NowaUsługaContract;
using ObsługaKlientaContract;
using RejestracjaContract;
using StatystykiContract;
using WysyłanieMailiContract;
using WyświetlContract;
using HistoriaTransakcjiContract;
using BazaDanychContract;
using NowaUsługaComponent;
using ObsługaKlientaComponent;
using RejestracjaComponent;
using StatystykiComponent;
using WysyłanieMailiComponent;
using WyświetlComponent;
using HistoriaTransakcjiComponent;
using Baza_Danych_Component;
using System.Text.RegularExpressions;
using TypOperacji;
using System.Reflection;
using System.Data.SqlClient;
#endregion


namespace Projekt_CRM_
{
    public partial class EdytujKlientaOkno : UserControl
    {
        private SqlDataReader Czytacz;
        private List<int> ListaIndeksów;
        private int wybranyklient;
        private bool wybieranie = false;
        public EdytujKlientaOkno()
        {
            ListaIndeksów = new List<int>();
            InitializeComponent();
            WypełnijCombo();
        }

        private void CofnijButton_Click(object sender, EventArgs e)
        {
            CzyszczeniePólTekstowych();
            ComboKlienci.SelectedItem = null;
            this.Visible = false;
        }

        private void EdytujButton_Click(object sender, EventArgs e)
        {
            if (ComboKlienci.SelectedIndex != -1)
            {
                wybranyklient = ListaIndeksów[ComboKlienci.SelectedIndex];
                Czytacz = (MainWindow.DOSTĘPNEOPERACJE[3] as IWyświetl).WyświetlDanePersonalne(wybranyklient);
                Czytacz.Read();
                CzyszczeniePólTekstowych();

                Imię.Text = Czytacz[1].ToString();
                Nazwisko.Text = Czytacz[2].ToString();
                Pesel.Text = Czytacz[3].ToString();
                Nip.Text = Czytacz[4].ToString();
                KodPocztowy.Text = Czytacz[5].ToString();
                Miasto.Text = Czytacz[6].ToString();
                Ulica.Text = Czytacz[7].ToString();
                NumerDomu.Text = Czytacz[8].ToString();
                NumerMieszkania.Text = Czytacz[9].ToString();
                EMail.Text = Czytacz[10].ToString();
                Telefon.Text = Czytacz[11].ToString();
                (MainWindow.DOSTĘPNEOPERACJE[3] as IWyświetl).PołączenieClose();
                wybieranie = true;
            }
        }

        private void ZatwierdźEdytujButton_Click(object sender, EventArgs e)
        {
            if (ComboKlienci.SelectedIndex >-1 && wybieranie == true)
            {
                string PolecenieSql = String.Format(
                                  @"UPDATE [dbo].[Klienci]
                                  SET [Imię/NazwaFirmy]='{0}',[Nazwisko]='{1}',[PESEL/REGON]='{2}',[NIP]='{3}',[Kod]='{4}',[Miasto]='{5}',[Ulica]='{6}',[NumerDomu]='{7}',[NumerMieszkania]='{8}' ,[EMail]='{9}',[Telefon]='{10}'
                                  WHERE IdKlienta = '{11}'", Imię.Text, Nazwisko.Text, Pesel.Text, Nip.Text, KodPocztowy.Text, Miasto.Text, Ulica.Text, NumerDomu.Text, NumerMieszkania.Text, EMail.Text, Telefon.Text, wybranyklient);
                try
                {
                    (MainWindow.DOSTĘPNEOPERACJE[2] as IObsługaKlienta).EdytujKlienta(PolecenieSql);
                    WypełnijCombo();

                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message);
                }
                ComboKlienci.Text = "";
                CzyszczeniePólTekstowych();
            }

        }

        private void CzyszczeniePólTekstowych()
        {
            Imię.Text = "";
            Nazwisko.Text = "";
            Pesel.Text = "";
            Nip.Text = "";
            KodPocztowy.Text = "";
            Miasto.Text = "";
            Ulica.Text = "";
            NumerDomu.Text = "";
            NumerMieszkania.Text = "";
            EMail.Text = "";
            Telefon.Text = "";
        }
        private void WypełnijCombo()
        {
            ComboKlienci.Items.Clear();
            ComboKlienci.SelectedItem = null;
            Czytacz = (MainWindow.DOSTĘPNEOPERACJE[3] as IWyświetl).WyświetlWszystkichKlientów();
            if (Czytacz.HasRows)
            {
                while (Czytacz.Read())
                {
                    ListaIndeksów.Add(Convert.ToInt32(Czytacz[0]));
                    ComboKlienci.Items.Add("Id: " + Czytacz[0] + " " + Czytacz[1] +" "+ Czytacz[2] + " Pesel: " + Czytacz[3]);
                }
            }
            (MainWindow.DOSTĘPNEOPERACJE[3] as IWyświetl).PołączenieClose();
        }
    }
}
