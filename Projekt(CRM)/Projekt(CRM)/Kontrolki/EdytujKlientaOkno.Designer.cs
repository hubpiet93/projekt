﻿namespace Projekt_CRM_
{
    partial class EdytujKlientaOkno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CofnijButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ComboKlienci = new System.Windows.Forms.ComboBox();
            this.EdytujButton = new System.Windows.Forms.Button();
            this.ZatwierdźEdytujButton = new System.Windows.Forms.Button();
            this.Telefon = new System.Windows.Forms.TextBox();
            this.EMail = new System.Windows.Forms.TextBox();
            this.NumerMieszkania = new System.Windows.Forms.TextBox();
            this.NumerDomu = new System.Windows.Forms.TextBox();
            this.Ulica = new System.Windows.Forms.TextBox();
            this.Miasto = new System.Windows.Forms.TextBox();
            this.KodPocztowy = new System.Windows.Forms.TextBox();
            this.Nip = new System.Windows.Forms.TextBox();
            this.Pesel = new System.Windows.Forms.TextBox();
            this.Nazwisko = new System.Windows.Forms.TextBox();
            this.Imię = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CofnijButton
            // 
            this.CofnijButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CofnijButton.Location = new System.Drawing.Point(3, 3);
            this.CofnijButton.Name = "CofnijButton";
            this.CofnijButton.Size = new System.Drawing.Size(75, 23);
            this.CofnijButton.TabIndex = 1;
            this.CofnijButton.Text = "Cofnij";
            this.CofnijButton.UseVisualStyleBackColor = true;
            this.CofnijButton.Click += new System.EventHandler(this.CofnijButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(253, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Edytuj dane klienta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(129, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Wybierz klienta:";
            // 
            // ComboKlienci
            // 
            this.ComboKlienci.FormattingEnabled = true;
            this.ComboKlienci.Location = new System.Drawing.Point(132, 93);
            this.ComboKlienci.Name = "ComboKlienci";
            this.ComboKlienci.Size = new System.Drawing.Size(218, 21);
            this.ComboKlienci.TabIndex = 4;
            // 
            // EdytujButton
            // 
            this.EdytujButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EdytujButton.Location = new System.Drawing.Point(390, 93);
            this.EdytujButton.Name = "EdytujButton";
            this.EdytujButton.Size = new System.Drawing.Size(83, 23);
            this.EdytujButton.TabIndex = 5;
            this.EdytujButton.Text = "Edytuj";
            this.EdytujButton.UseVisualStyleBackColor = true;
            this.EdytujButton.Click += new System.EventHandler(this.EdytujButton_Click);
            // 
            // ZatwierdźEdytujButton
            // 
            this.ZatwierdźEdytujButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ZatwierdźEdytujButton.Location = new System.Drawing.Point(197, 526);
            this.ZatwierdźEdytujButton.Name = "ZatwierdźEdytujButton";
            this.ZatwierdźEdytujButton.Size = new System.Drawing.Size(200, 40);
            this.ZatwierdźEdytujButton.TabIndex = 46;
            this.ZatwierdźEdytujButton.Text = "Zapisz zmiany";
            this.ZatwierdźEdytujButton.UseVisualStyleBackColor = true;
            this.ZatwierdźEdytujButton.Click += new System.EventHandler(this.ZatwierdźEdytujButton_Click);
            // 
            // Telefon
            // 
            this.Telefon.Location = new System.Drawing.Point(289, 488);
            this.Telefon.Name = "Telefon";
            this.Telefon.Size = new System.Drawing.Size(184, 20);
            this.Telefon.TabIndex = 45;
            // 
            // EMail
            // 
            this.EMail.Location = new System.Drawing.Point(289, 452);
            this.EMail.Name = "EMail";
            this.EMail.Size = new System.Drawing.Size(184, 20);
            this.EMail.TabIndex = 44;
            // 
            // NumerMieszkania
            // 
            this.NumerMieszkania.Location = new System.Drawing.Point(289, 416);
            this.NumerMieszkania.Name = "NumerMieszkania";
            this.NumerMieszkania.Size = new System.Drawing.Size(184, 20);
            this.NumerMieszkania.TabIndex = 43;
            // 
            // NumerDomu
            // 
            this.NumerDomu.Location = new System.Drawing.Point(289, 380);
            this.NumerDomu.Name = "NumerDomu";
            this.NumerDomu.Size = new System.Drawing.Size(184, 20);
            this.NumerDomu.TabIndex = 42;
            // 
            // Ulica
            // 
            this.Ulica.Location = new System.Drawing.Point(289, 344);
            this.Ulica.Name = "Ulica";
            this.Ulica.Size = new System.Drawing.Size(184, 20);
            this.Ulica.TabIndex = 41;
            // 
            // Miasto
            // 
            this.Miasto.Location = new System.Drawing.Point(289, 308);
            this.Miasto.Name = "Miasto";
            this.Miasto.Size = new System.Drawing.Size(184, 20);
            this.Miasto.TabIndex = 40;
            // 
            // KodPocztowy
            // 
            this.KodPocztowy.Location = new System.Drawing.Point(289, 272);
            this.KodPocztowy.Name = "KodPocztowy";
            this.KodPocztowy.Size = new System.Drawing.Size(184, 20);
            this.KodPocztowy.TabIndex = 39;
            // 
            // Nip
            // 
            this.Nip.Location = new System.Drawing.Point(289, 236);
            this.Nip.Name = "Nip";
            this.Nip.Size = new System.Drawing.Size(184, 20);
            this.Nip.TabIndex = 38;
            // 
            // Pesel
            // 
            this.Pesel.Location = new System.Drawing.Point(289, 200);
            this.Pesel.Name = "Pesel";
            this.Pesel.Size = new System.Drawing.Size(184, 20);
            this.Pesel.TabIndex = 37;
            // 
            // Nazwisko
            // 
            this.Nazwisko.Location = new System.Drawing.Point(289, 164);
            this.Nazwisko.Name = "Nazwisko";
            this.Nazwisko.Size = new System.Drawing.Size(184, 20);
            this.Nazwisko.TabIndex = 36;
            // 
            // Imię
            // 
            this.Imię.Location = new System.Drawing.Point(289, 131);
            this.Imię.Name = "Imię";
            this.Imię.Size = new System.Drawing.Size(184, 20);
            this.Imię.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(133, 491);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 17);
            this.label11.TabIndex = 34;
            this.label11.Text = "Telefon: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(133, 455);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 17);
            this.label10.TabIndex = 33;
            this.label10.Text = "Adres E-Mail: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(133, 419);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 17);
            this.label9.TabIndex = 32;
            this.label9.Text = "Numer mieszkania:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(133, 383);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 17);
            this.label5.TabIndex = 31;
            this.label5.Text = "Numer domu:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(133, 347);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 17);
            this.label6.TabIndex = 30;
            this.label6.Text = "Ulica: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(133, 311);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 17);
            this.label7.TabIndex = 29;
            this.label7.Text = "Miasto:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(133, 275);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 17);
            this.label8.TabIndex = 28;
            this.label8.Text = "Kod pocztowy:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(133, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 17);
            this.label4.TabIndex = 27;
            this.label4.Text = "NIP:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(133, 203);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 17);
            this.label3.TabIndex = 26;
            this.label3.Text = "PESEL / REGON:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(133, 167);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 17);
            this.label12.TabIndex = 25;
            this.label12.Text = "Nazwisko:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(133, 131);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(129, 17);
            this.label13.TabIndex = 24;
            this.label13.Text = "Imię / Nazwa Firmy:";
            // 
            // EdytujKlientaOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ZatwierdźEdytujButton);
            this.Controls.Add(this.Telefon);
            this.Controls.Add(this.EMail);
            this.Controls.Add(this.NumerMieszkania);
            this.Controls.Add(this.NumerDomu);
            this.Controls.Add(this.Ulica);
            this.Controls.Add(this.Miasto);
            this.Controls.Add(this.KodPocztowy);
            this.Controls.Add(this.Nip);
            this.Controls.Add(this.Pesel);
            this.Controls.Add(this.Nazwisko);
            this.Controls.Add(this.Imię);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.EdytujButton);
            this.Controls.Add(this.ComboKlienci);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CofnijButton);
            this.Name = "EdytujKlientaOkno";
            this.Size = new System.Drawing.Size(660, 580);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CofnijButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ComboKlienci;
        private System.Windows.Forms.Button EdytujButton;
        private System.Windows.Forms.Button ZatwierdźEdytujButton;
        private System.Windows.Forms.TextBox Telefon;
        private System.Windows.Forms.TextBox EMail;
        private System.Windows.Forms.TextBox NumerMieszkania;
        private System.Windows.Forms.TextBox NumerDomu;
        private System.Windows.Forms.TextBox Ulica;
        private System.Windows.Forms.TextBox Miasto;
        private System.Windows.Forms.TextBox KodPocztowy;
        private System.Windows.Forms.TextBox Nip;
        private System.Windows.Forms.TextBox Pesel;
        private System.Windows.Forms.TextBox Nazwisko;
        private System.Windows.Forms.TextBox Imię;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}
