﻿#region//biblioteki
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PK.Container;
using NowaUsługaContract;
using ObsługaKlientaContract;
using RejestracjaContract;
using StatystykiContract;
using WysyłanieMailiContract;
using WyświetlContract;
using HistoriaTransakcjiContract;
using BazaDanychContract;
using NowaUsługaComponent;
using ObsługaKlientaComponent;
using RejestracjaComponent;
using StatystykiComponent;
using WysyłanieMailiComponent;
using WyświetlComponent;
using HistoriaTransakcjiComponent;
using Baza_Danych_Component;
using System.Text.RegularExpressions;
using TypOperacji;
using System.Reflection;
using System.Data.SqlClient;
#endregion


namespace Projekt_CRM_.Kontrolki
{
    public partial class WyświetlanieOkno : UserControl
    {
        private SqlDataReader Czytacz;
        public WyświetlanieOkno()
        {
            InitializeComponent();
        }

        private void WyświetlButton_Click(object sender, EventArgs e)
        {
            ListaKlientów.Items.Clear();
            Czytacz = (MainWindow.DOSTĘPNEOPERACJE[3] as IWyświetl).WyświetlWszystkichKlientów();
            if (Czytacz.HasRows)
            {
                while (Czytacz.Read())
                {
                   ListViewItem listitems = new ListViewItem(Czytacz[0].ToString());
                   listitems.SubItems.Add(Czytacz[1].ToString());
                   listitems.SubItems.Add(Czytacz[2].ToString());
                   listitems.SubItems.Add(Czytacz[3].ToString());
                   listitems.SubItems.Add(Czytacz[4].ToString());
                   listitems.SubItems.Add(Czytacz[5].ToString());
                   listitems.SubItems.Add(Czytacz[6].ToString());
                   listitems.SubItems.Add(Czytacz[7].ToString());
                   listitems.SubItems.Add(Czytacz[8].ToString());
                   listitems.SubItems.Add(Czytacz[9].ToString());
                   listitems.SubItems.Add(Czytacz[10].ToString());
                   listitems.SubItems.Add(Czytacz[11].ToString());
                   ListaKlientów.Items.Add(listitems);
                }                
            }
            (MainWindow.DOSTĘPNEOPERACJE[3] as IWyświetl).PołączenieClose();
        }
    }
}
