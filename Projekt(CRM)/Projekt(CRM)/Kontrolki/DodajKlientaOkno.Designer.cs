﻿namespace Projekt_CRM_
{
    partial class DodajKlientaOkno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CofnijButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Imię = new System.Windows.Forms.TextBox();
            this.Nazwisko = new System.Windows.Forms.TextBox();
            this.Pesel = new System.Windows.Forms.TextBox();
            this.Nip = new System.Windows.Forms.TextBox();
            this.KodPocztowy = new System.Windows.Forms.TextBox();
            this.Miasto = new System.Windows.Forms.TextBox();
            this.Ulica = new System.Windows.Forms.TextBox();
            this.NumerDomu = new System.Windows.Forms.TextBox();
            this.NumerMieszkania = new System.Windows.Forms.TextBox();
            this.EMail = new System.Windows.Forms.TextBox();
            this.Telefon = new System.Windows.Forms.TextBox();
            this.DodajButton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.PoprawneImię = new System.Windows.Forms.Label();
            this.PoprawneNazwisko = new System.Windows.Forms.Label();
            this.PoprawnyPesel = new System.Windows.Forms.Label();
            this.PoprawneMiasto = new System.Windows.Forms.Label();
            this.PoprawnyKod = new System.Windows.Forms.Label();
            this.PoprawnyNip = new System.Windows.Forms.Label();
            this.PoprawneMieszkanie = new System.Windows.Forms.Label();
            this.PoprawnyDom = new System.Windows.Forms.Label();
            this.PoprawnaUlica = new System.Windows.Forms.Label();
            this.PoprawnyTelefon = new System.Windows.Forms.Label();
            this.PoprawnyEmail = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CofnijButton
            // 
            this.CofnijButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CofnijButton.Location = new System.Drawing.Point(3, 3);
            this.CofnijButton.Name = "CofnijButton";
            this.CofnijButton.Size = new System.Drawing.Size(75, 23);
            this.CofnijButton.TabIndex = 0;
            this.CofnijButton.Text = "Cofnij";
            this.CofnijButton.UseVisualStyleBackColor = true;
            this.CofnijButton.Click += new System.EventHandler(this.CofnijButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(155, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Imię / Nazwa Firmy:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(155, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nazwisko:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(155, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "PESEL / REGON:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(155, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "NIP:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(155, 336);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Numer domu:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(155, 300);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Ulica: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(155, 264);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Miasto:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(155, 228);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 17);
            this.label8.TabIndex = 5;
            this.label8.Text = "Kod pocztowy:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(155, 372);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 17);
            this.label9.TabIndex = 9;
            this.label9.Text = "Numer mieszkania:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(155, 408);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 17);
            this.label10.TabIndex = 10;
            this.label10.Text = "Adres E-Mail: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(155, 444);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 17);
            this.label11.TabIndex = 11;
            this.label11.Text = "Telefon: ";
            // 
            // Imię
            // 
            this.Imię.Location = new System.Drawing.Point(311, 84);
            this.Imię.Name = "Imię";
            this.Imię.Size = new System.Drawing.Size(124, 20);
            this.Imię.TabIndex = 12;
            // 
            // Nazwisko
            // 
            this.Nazwisko.Location = new System.Drawing.Point(311, 117);
            this.Nazwisko.Name = "Nazwisko";
            this.Nazwisko.Size = new System.Drawing.Size(124, 20);
            this.Nazwisko.TabIndex = 13;
            // 
            // Pesel
            // 
            this.Pesel.Location = new System.Drawing.Point(311, 153);
            this.Pesel.Name = "Pesel";
            this.Pesel.Size = new System.Drawing.Size(124, 20);
            this.Pesel.TabIndex = 14;
            // 
            // Nip
            // 
            this.Nip.Location = new System.Drawing.Point(311, 189);
            this.Nip.Name = "Nip";
            this.Nip.Size = new System.Drawing.Size(124, 20);
            this.Nip.TabIndex = 15;
            // 
            // KodPocztowy
            // 
            this.KodPocztowy.Location = new System.Drawing.Point(311, 225);
            this.KodPocztowy.Name = "KodPocztowy";
            this.KodPocztowy.Size = new System.Drawing.Size(124, 20);
            this.KodPocztowy.TabIndex = 16;
            // 
            // Miasto
            // 
            this.Miasto.Location = new System.Drawing.Point(311, 261);
            this.Miasto.Name = "Miasto";
            this.Miasto.Size = new System.Drawing.Size(124, 20);
            this.Miasto.TabIndex = 17;
            // 
            // Ulica
            // 
            this.Ulica.Location = new System.Drawing.Point(311, 297);
            this.Ulica.Name = "Ulica";
            this.Ulica.Size = new System.Drawing.Size(124, 20);
            this.Ulica.TabIndex = 18;
            // 
            // NumerDomu
            // 
            this.NumerDomu.Location = new System.Drawing.Point(311, 333);
            this.NumerDomu.Name = "NumerDomu";
            this.NumerDomu.Size = new System.Drawing.Size(124, 20);
            this.NumerDomu.TabIndex = 19;
            // 
            // NumerMieszkania
            // 
            this.NumerMieszkania.Location = new System.Drawing.Point(311, 369);
            this.NumerMieszkania.Name = "NumerMieszkania";
            this.NumerMieszkania.Size = new System.Drawing.Size(124, 20);
            this.NumerMieszkania.TabIndex = 20;
            // 
            // EMail
            // 
            this.EMail.Location = new System.Drawing.Point(311, 405);
            this.EMail.Name = "EMail";
            this.EMail.Size = new System.Drawing.Size(124, 20);
            this.EMail.TabIndex = 21;
            // 
            // Telefon
            // 
            this.Telefon.Location = new System.Drawing.Point(311, 441);
            this.Telefon.Name = "Telefon";
            this.Telefon.Size = new System.Drawing.Size(124, 20);
            this.Telefon.TabIndex = 22;
            // 
            // DodajButton
            // 
            this.DodajButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DodajButton.Location = new System.Drawing.Point(219, 479);
            this.DodajButton.Name = "DodajButton";
            this.DodajButton.Size = new System.Drawing.Size(140, 40);
            this.DodajButton.TabIndex = 23;
            this.DodajButton.Text = "Dodaj";
            this.DodajButton.UseVisualStyleBackColor = true;
            this.DodajButton.Click += new System.EventHandler(this.DodajButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(245, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 17);
            this.label12.TabIndex = 24;
            this.label12.Text = "Dodaj klienta";
            // 
            // PoprawneImię
            // 
            this.PoprawneImię.AutoSize = true;
            this.PoprawneImię.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawneImię.ForeColor = System.Drawing.Color.Red;
            this.PoprawneImię.Location = new System.Drawing.Point(441, 88);
            this.PoprawneImię.Name = "PoprawneImię";
            this.PoprawneImię.Size = new System.Drawing.Size(0, 20);
            this.PoprawneImię.TabIndex = 25;
            // 
            // PoprawneNazwisko
            // 
            this.PoprawneNazwisko.AutoSize = true;
            this.PoprawneNazwisko.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawneNazwisko.ForeColor = System.Drawing.Color.Red;
            this.PoprawneNazwisko.Location = new System.Drawing.Point(441, 120);
            this.PoprawneNazwisko.Name = "PoprawneNazwisko";
            this.PoprawneNazwisko.Size = new System.Drawing.Size(0, 20);
            this.PoprawneNazwisko.TabIndex = 26;
            // 
            // PoprawnyPesel
            // 
            this.PoprawnyPesel.AutoSize = true;
            this.PoprawnyPesel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawnyPesel.ForeColor = System.Drawing.Color.Red;
            this.PoprawnyPesel.Location = new System.Drawing.Point(441, 156);
            this.PoprawnyPesel.Name = "PoprawnyPesel";
            this.PoprawnyPesel.Size = new System.Drawing.Size(0, 20);
            this.PoprawnyPesel.TabIndex = 27;
            // 
            // PoprawneMiasto
            // 
            this.PoprawneMiasto.AutoSize = true;
            this.PoprawneMiasto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawneMiasto.ForeColor = System.Drawing.Color.Red;
            this.PoprawneMiasto.Location = new System.Drawing.Point(441, 262);
            this.PoprawneMiasto.Name = "PoprawneMiasto";
            this.PoprawneMiasto.Size = new System.Drawing.Size(14, 20);
            this.PoprawneMiasto.TabIndex = 30;
            this.PoprawneMiasto.Text = " ";
            // 
            // PoprawnyKod
            // 
            this.PoprawnyKod.AutoSize = true;
            this.PoprawnyKod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawnyKod.ForeColor = System.Drawing.Color.Red;
            this.PoprawnyKod.Location = new System.Drawing.Point(441, 226);
            this.PoprawnyKod.Name = "PoprawnyKod";
            this.PoprawnyKod.Size = new System.Drawing.Size(0, 20);
            this.PoprawnyKod.TabIndex = 29;
            // 
            // PoprawnyNip
            // 
            this.PoprawnyNip.AutoSize = true;
            this.PoprawnyNip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawnyNip.ForeColor = System.Drawing.Color.Red;
            this.PoprawnyNip.Location = new System.Drawing.Point(441, 194);
            this.PoprawnyNip.Name = "PoprawnyNip";
            this.PoprawnyNip.Size = new System.Drawing.Size(0, 20);
            this.PoprawnyNip.TabIndex = 28;
            // 
            // PoprawneMieszkanie
            // 
            this.PoprawneMieszkanie.AutoSize = true;
            this.PoprawneMieszkanie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawneMieszkanie.ForeColor = System.Drawing.Color.Red;
            this.PoprawneMieszkanie.Location = new System.Drawing.Point(441, 369);
            this.PoprawneMieszkanie.Name = "PoprawneMieszkanie";
            this.PoprawneMieszkanie.Size = new System.Drawing.Size(14, 20);
            this.PoprawneMieszkanie.TabIndex = 33;
            this.PoprawneMieszkanie.Text = " ";
            // 
            // PoprawnyDom
            // 
            this.PoprawnyDom.AutoSize = true;
            this.PoprawnyDom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawnyDom.ForeColor = System.Drawing.Color.Red;
            this.PoprawnyDom.Location = new System.Drawing.Point(441, 333);
            this.PoprawnyDom.Name = "PoprawnyDom";
            this.PoprawnyDom.Size = new System.Drawing.Size(14, 20);
            this.PoprawnyDom.TabIndex = 32;
            this.PoprawnyDom.Text = " ";
            // 
            // PoprawnaUlica
            // 
            this.PoprawnaUlica.AutoSize = true;
            this.PoprawnaUlica.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawnaUlica.ForeColor = System.Drawing.Color.Red;
            this.PoprawnaUlica.Location = new System.Drawing.Point(441, 301);
            this.PoprawnaUlica.Name = "PoprawnaUlica";
            this.PoprawnaUlica.Size = new System.Drawing.Size(14, 20);
            this.PoprawnaUlica.TabIndex = 31;
            this.PoprawnaUlica.Text = " ";
            // 
            // PoprawnyTelefon
            // 
            this.PoprawnyTelefon.AutoSize = true;
            this.PoprawnyTelefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawnyTelefon.ForeColor = System.Drawing.Color.Red;
            this.PoprawnyTelefon.Location = new System.Drawing.Point(441, 441);
            this.PoprawnyTelefon.Name = "PoprawnyTelefon";
            this.PoprawnyTelefon.Size = new System.Drawing.Size(14, 20);
            this.PoprawnyTelefon.TabIndex = 35;
            this.PoprawnyTelefon.Text = " ";
            // 
            // PoprawnyEmail
            // 
            this.PoprawnyEmail.AutoSize = true;
            this.PoprawnyEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoprawnyEmail.ForeColor = System.Drawing.Color.Red;
            this.PoprawnyEmail.Location = new System.Drawing.Point(441, 409);
            this.PoprawnyEmail.Name = "PoprawnyEmail";
            this.PoprawnyEmail.Size = new System.Drawing.Size(14, 20);
            this.PoprawnyEmail.TabIndex = 34;
            this.PoprawnyEmail.Text = " ";
            // 
            // DodajKlientaOkno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PoprawnyTelefon);
            this.Controls.Add(this.PoprawnyEmail);
            this.Controls.Add(this.PoprawneMieszkanie);
            this.Controls.Add(this.PoprawnyDom);
            this.Controls.Add(this.PoprawnaUlica);
            this.Controls.Add(this.PoprawneMiasto);
            this.Controls.Add(this.PoprawnyKod);
            this.Controls.Add(this.PoprawnyNip);
            this.Controls.Add(this.PoprawnyPesel);
            this.Controls.Add(this.PoprawneNazwisko);
            this.Controls.Add(this.PoprawneImię);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.DodajButton);
            this.Controls.Add(this.Telefon);
            this.Controls.Add(this.EMail);
            this.Controls.Add(this.NumerMieszkania);
            this.Controls.Add(this.NumerDomu);
            this.Controls.Add(this.Ulica);
            this.Controls.Add(this.Miasto);
            this.Controls.Add(this.KodPocztowy);
            this.Controls.Add(this.Nip);
            this.Controls.Add(this.Pesel);
            this.Controls.Add(this.Nazwisko);
            this.Controls.Add(this.Imię);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CofnijButton);
            this.Name = "DodajKlientaOkno";
            this.Size = new System.Drawing.Size(660, 580);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CofnijButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Imię;
        private System.Windows.Forms.TextBox Nazwisko;
        private System.Windows.Forms.TextBox Pesel;
        private System.Windows.Forms.TextBox Nip;
        private System.Windows.Forms.TextBox KodPocztowy;
        private System.Windows.Forms.TextBox Miasto;
        private System.Windows.Forms.TextBox Ulica;
        private System.Windows.Forms.TextBox NumerDomu;
        private System.Windows.Forms.TextBox NumerMieszkania;
        private System.Windows.Forms.TextBox EMail;
        private System.Windows.Forms.TextBox Telefon;
        private System.Windows.Forms.Button DodajButton;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label PoprawneImię;
        private System.Windows.Forms.Label PoprawneNazwisko;
        private System.Windows.Forms.Label PoprawnyPesel;
        private System.Windows.Forms.Label PoprawneMiasto;
        private System.Windows.Forms.Label PoprawnyKod;
        private System.Windows.Forms.Label PoprawnyNip;
        private System.Windows.Forms.Label PoprawneMieszkanie;
        private System.Windows.Forms.Label PoprawnyDom;
        private System.Windows.Forms.Label PoprawnaUlica;
        private System.Windows.Forms.Label PoprawnyTelefon;
        private System.Windows.Forms.Label PoprawnyEmail;
    }
}
