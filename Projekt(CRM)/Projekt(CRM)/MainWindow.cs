﻿#region//biblioteki
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PK.Container;
using NowaUsługaContract;
using ObsługaKlientaContract;
using RejestracjaContract;
using StatystykiContract;
using WysyłanieMailiContract;
using WyświetlContract;
using HistoriaTransakcjiContract;
using BazaDanychContract;
using NowaUsługaComponent;
using ObsługaKlientaComponent;
using RejestracjaComponent;
using StatystykiComponent;
using WysyłanieMailiComponent;
using WyświetlComponent;
using HistoriaTransakcjiComponent;
using Baza_Danych_Component;
using System.Data.SqlClient;
using TypOperacji;
using System.Reflection;
#endregion

namespace Projekt_CRM_
{ 
    public partial class MainWindow : Form
    {
        private List<UserControl> Okienka = new List<UserControl>();
        private static IList<IOperacja> DostępneOperacje;

        public MainWindow()
        {
            InitializeComponent();      
            #region//Tworzenie listy okienek
            groupBox1.Visible = true;
            Okienka.Add(wyświetlanieOkno1);
            Okienka.Add(dodajKlientaOkno1);
            Okienka.Add(edytujKlientaOkno1);
            Okienka.Add(nowaUsługaOkno1);
            Okienka.Add(historiaTransakcjiOkno1);
            Okienka.Add(statystykiOkno1);
            Okienka.Add(daneKlientaOkno1);
            Okienka.Add(wyślijMailaOkno1);
            Okienka.Add(dodajTransakcjęOkno1);
            #endregion
        }


        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        private void DodajKlientaButton_Click(object sender, EventArgs e)
        {
            AktywujOkienko(dodajKlientaOkno1, Okienka);
        }
        private void koniecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void edytujKlientaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AktywujOkienko(edytujKlientaOkno1, Okienka);
        }
        private void nowaUsługaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AktywujOkienko(nowaUsługaOkno1, Okienka);
        }
        private void historiaTransakcjiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AktywujOkienko(historiaTransakcjiOkno1, Okienka);
        }
        private void statystykiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AktywujOkienko(statystykiOkno1, Okienka);
        }
        private void wyświetlDaneKlientaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AktywujOkienko(daneKlientaOkno1, Okienka);
        }
        private void wyślijMailaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AktywujOkienko(wyślijMailaOkno1, Okienka);
        }
        private void dodajTransakcjęToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AktywujOkienko(dodajTransakcjęOkno1, Okienka);
        }
        private void WyświetlDaneButton_Click(object sender, EventArgs e)
        {
            AktywujOkienko(wyświetlanieOkno1, Okienka);
        }

        //pomocnicze metody
        public static void AktywujOkienko(UserControl oknodoaktywacji, List<UserControl> Okienka)
        {
            foreach (UserControl okno in Okienka)
            {
                if (okno == oknodoaktywacji)
                {
                    okno.Visible = true;
                }
                else
                {
                    okno.Visible = false;
                }
            }
           
        }
        public static IList<IOperacja> DOSTĘPNEOPERACJE
        {
            get
            {
                return DostępneOperacje;
            }
            set
            {
                DostępneOperacje = value;
            }
        }

      



        
    }
}
