﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Web.Security;
namespace WysyłanieMailiComponent
{
    public class WysyłanieMailiImplementacja : WysyłanieMailiContract.IOperacjeMail
    {

        public WysyłanieMailiImplementacja()
        {
 
        }

        public void PojedyńczyMail(string AdresNadawcy, string Hasło, string SMTPAdres, int Port, string AdresOdbiorcy, string TematWiadomości, string Wiadomość)
        {
            try
            {
                MailMessage mail = new MailMessage(AdresNadawcy, AdresOdbiorcy, TematWiadomości, Wiadomość);
                SmtpClient klient = new SmtpClient(SMTPAdres);

                klient.Port = Port;
                klient.Credentials = new NetworkCredential(AdresNadawcy, Hasło);
                klient.EnableSsl = true;
                klient.Send(mail);
            }
            catch (Exception)
            {
            }
        }
        public void SeryjnyMail(string AdresNadawcy, string Hasło, string SMTPAdres, int Port, List<string> AdresOdbiorcy, string TematWiadomości, string Wiadomość)
        {
            try
            {
                SmtpClient klient = new SmtpClient(SMTPAdres);
                klient.Credentials = new NetworkCredential(AdresNadawcy, Hasło);
                klient.Port = Port;
                klient.EnableSsl = true;

                foreach (String adresobiorcy in AdresOdbiorcy)
                {
                    MailMessage mail = new MailMessage(AdresNadawcy, adresobiorcy, TematWiadomości, Wiadomość);
                    klient.Send(mail);
                }
                
            }
            catch (Exception)
            {
            }
        }
    }
}
