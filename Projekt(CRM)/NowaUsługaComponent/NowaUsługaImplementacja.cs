﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace NowaUsługaComponent
{
    public class NowaUsługaImplementacja : NowaUsługaContract.IDodajUsługę
    {
        private SqlConnection Połączenie;

        public NowaUsługaImplementacja(SqlConnection Połączenie)
        {
            this.Połączenie = Połączenie;
        }


        public void DodajNowąUsługę(string NazwaUsługi)
        {
            SqlCommand kwerenda;
            string Polecenie;
            try
            {
                Polecenie = String.Format("insert into [dbo].[Usługi] (NazwaUsługi) values ('{0}')", NazwaUsługi);
                kwerenda = new SqlCommand(Polecenie);
                Połączenie.Open();               
                kwerenda.Connection = Połączenie;
                kwerenda.ExecuteNonQuery();
                Połączenie.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }


        public SqlDataReader GetUsługi()
        {
            SqlDataReader Czytnik;
            SqlCommand Kwerenda;
            string Polecenie;
            try
            {
                Połączenie.Open();
                Polecenie = String.Format("select * from [dbo].[Usługi]");
                Kwerenda = new SqlCommand(Polecenie);
                Kwerenda.Connection = Połączenie;
                Kwerenda.Clone();
                Czytnik = Kwerenda.ExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Czytnik = null;
            }
            return Czytnik;
        }


        public void PołączenieClose()
        {
            Połączenie.Close();
        }
    }
}
