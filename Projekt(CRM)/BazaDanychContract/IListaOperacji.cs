﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazaDanychContract
{
    public interface IListaOperacji
    {
        IList<TypOperacji.IOperacja> ListaOperacji{get;set;}
    }
}
