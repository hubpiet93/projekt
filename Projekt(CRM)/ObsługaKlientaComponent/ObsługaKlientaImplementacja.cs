﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace ObsługaKlientaComponent
{
    public class ObsługaKlientaImplementacja : ObsługaKlientaContract.IObsługaKlienta
    {
        private SqlConnection Połączenie;

        public ObsługaKlientaImplementacja(SqlConnection Połączenie)
        {
            this.Połączenie = Połączenie;
        }

        public void DodajTransakcję(SqlCommand Polecenie)
        {
            try
            {
                
                Połączenie.Open();
                Polecenie.Connection = Połączenie;          
                Polecenie.ExecuteNonQuery();
                Połączenie.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public void EdytujKlienta(string Polecenie)
        {
            SqlCommand kwerenda;
            try
            {
                kwerenda = new SqlCommand(Polecenie);
                Połączenie.Open();
                kwerenda.Connection = Połączenie;
                // Połączenie.Clone();
                kwerenda.ExecuteNonQuery();
                Połączenie.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
